// Canvas variables
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');
const width = 1000;
const height = 400;

// Live speed slider variable and updater function
let speed = document.getElementById('speed').value;
function changeSpeed(val) {
    document.getElementById("speed").innerHTML = val;
    speed = val;
}

// Canvas properties
canvas.height = height;
canvas.width = width;

// Array variables
const arrayLength = 88;
let array = [];

// Sorting variables
let algorithm = 'bubble';
let isSorting = false;
let i = 0;
let j = 0;

generateArray();
drawArray();

// const synth = new Tone.Synth().toDestination();

// let notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B",];
// const octaves = 8;
// let tones = [];
// for (let i = 0; i < octaves; i++) {
//     notes.forEach(note => {
//         tones.push(note+(i));
//     });
// }

// Check if value is in array
function inArray(array, vl) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] == vl) {
            return true;
        }
        return false;
    }
}

// Check if array is sorted
async function isSorted(arr) {
    let second_index;
    for (let first_index = 0; first_index < arr.length; first_index++) {
        second_index = first_index + 1;
        if (arr[second_index] - arr[first_index] < 0) return false;
        await sleep(speed);
        drawArray(first_index);
    }
    return true;
}

// Generate sorted array based on arraylengt variable 
function generateArray() {
    array = [];
    for (let i = 0; i < arrayLength; i++) {
        array.push((i * 4.6) + 1);
    }
}

function drawArray(cur, opt) {
    ctx.clearRect(0, 0, width, height);
    for (let i = 0; i < arrayLength; i++) {
        if (cur == i) {
            ctx.fillStyle = '#f0f';
        } else if (opt == i) {
            ctx.fillStyle = '#0f0';
        } else {
            ctx.fillStyle = '#303030';
        }
        ctx.fillRect(i * (width / arrayLength), height - array[i], (width / arrayLength) - 1, array[i]);
    }
}

async function swap(i, j) {
    await sleep(speed);
    let temp = array[i];
    array[i] = array[j];
    array[j] = temp;
    drawArray(i, j);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function shuffle() {
    for (let i = 0; i < arrayLength; i++) {
        randomIndex = Math.floor(Math.random() * arrayLength);
        await swap(i, randomIndex);
    }
    isSorting = false;
    drawArray();
}

async function reverse() {
    for (let i = 0; i < (arrayLength / 2); i++) {
        await swap(i, arrayLength - i - 1);
    }
    isSorting = false;
    drawArray();
}

async function bubbleSort() {
    for (let i = 0; i < arrayLength - 1; i++) {
        for (let j = 0; j < arrayLength - i - 1; j++) {
            if (array[j] > array[j + 1]) {
                await swap(j, j + 1);
            } else {
                await sleep(speed);
                drawArray(j, j + 1);
            }
        }
    }
}

async function selectionSort() {
    for (let i = 0; i < arrayLength - 1; i++) {
        min_index = i;
        for (let j = i + 1; j < arrayLength; j++) {
            if (array[j] < array[min_index]) {
                min_index = j;
                await sleep(speed);
                drawArray(min_index, j);
            } else {
                await sleep(speed);
                drawArray(min_index, j);
            }
        }
        await swap(min_index, i);
    }
}

async function insertionSort() {
    for (let i = 0; i < arrayLength; i++) {
        j = i;
        while (j > 0 && array[j - 1] > array[j]) {
            await swap(j, j - 1);
            j = j - 1;
        }
    }
}

async function cocktailSort() {
    let isSwapped = true;
    let start = 0;
    let end = array.length - 1;

    while (isSwapped) {
        // reset flag
        isSwapped = false;
        // traverse arrayay from left to right
        for (let i = start; i < end; i++) {
            if (array[i] > array[i + 1]) {
                // swap elements
                await swap(i+1, i);
                // [array[i], array[i + 1]] = [array[i + 1], array[i]];
                isSwapped = true;
            }
        }
        // if no swaps occurred, break
        if (!isSwapped) {
            break;
        }
        // update end position
        end--;
        // traverse array from right to left
        for (let i = end; i > start; i--) {
            if (array[i] < array[i - 1]) {
                // swap elements
                await swap(i-1, i);
                // [array[i], array[i - 1]] = [array[i - 1], array[i]];
                isSwapped = true;
            }
        }
        // update start position
        start++;
    }
}

async function mergeSort(arr, start, end) {
    if (start < end) {
        const mid = Math.floor((start + end) / 2);
        await mergeSort(arr, start, mid);
        await mergeSort(arr, mid + 1, end);
        await merge(arr, start, mid, end);
    }
}

async function merge(arr, start, mid, end) {
    const leftArr = arr.slice(start, mid + 1);
    const rightArr = arr.slice(mid + 1, end + 1);

    let i = 0, j = 0, k = start;
    while (i < leftArr.length && j < rightArr.length) {
        if (leftArr[i] <= rightArr[j]) {
            arr[k++] = leftArr[i++];
        } else {
            arr[k++] = rightArr[j++];
        }
        await sleep(speed); // sleep for 1 second to visualize the sorting process
        array = arr;
        drawArray(k, k + rightArr.length);
    }

    while (i < leftArr.length) {
        arr[k++] = leftArr[i++];
        await sleep(speed);
    }

    while (j < rightArr.length) {
        arr[k++] = rightArr[j++];
        await sleep(speed);
    }
    array = arr;
    drawArray(i + k, j + k);
}

async function heapSort() {
    // Build max heap
    for (let i = Math.floor(array.length / 2) - 1; i >= 0; i--) {
        await heapify(array.length, i);
    }

    // Extract elements from heap
    for (let i = array.length - 1; i > 0; i--) {
        // Move current root to end
        await swap(array[0], i);

        // Heapify reduced heap
        await heapify(i, 0);
    }

    return array;
}

async function heapify(n, i) {
    let largest = i; // Initialize largest as root
    let left = 2 * i + 1; // Left child
    let right = 2 * i + 2; // Right child

    // If left child is larger than root
    if (left < n && array[left] > array[largest]) {
        largest = left;
    }

    // If right child is larger than largest so far
    if (right < n && array[right] > array[largest]) {
        largest = right;
    }

    // If largest is not root
    if (largest !== i) {
        // Swap root with largest
        await swap(array[i], largest);

        // Recursively heapify the affected sub-tree
        await heapify(array[n], largest);
    }
}

async function quickSort(left = 0, right = array.length - 1) {
    if (left < right) {
        // Partition the array and get the pivot index
        const pivotIndex = await partition(array, left, right);

        // Recursively sort the left and right sub-arrays
        await Promise.all([
            quickSort(array, left, pivotIndex - 1),
            quickSort(array, pivotIndex + 1, right)
        ]);
    }
}

async function partition(array, left, right) {
    // Choose the pivot as the last element
    const pivot = array[right];

    // Initialize the pivot index as the leftmost index
    let pivotIndex = left;

    // Iterate over the array from left to right
    for (let i = left; i < right; i++) {
        // If the current element is less than or equal to the pivot
        if (array[i] <= pivot) {
            // Swap the current element with the element at the pivot index
            await swap(array, i, pivotIndex);

            // Increment the pivot index
            pivotIndex++;
        }
    }

    // Swap the pivot with the element at the pivot index
    await swap(array, right, pivotIndex);

    // Return the pivot index
    return pivotIndex;
}

async function shellSort() {

}

function sort() {
    if (isSorting) {
        return;
    }
    algorithm = document.getElementById('algorithm').value;
    isSorting = true;

    switch (algorithm) {
        case 'bubble':
            bubbleSort().then(() => {
                isSorting = false;
                // isSorted(array);
                drawArray();
            });
            break;
        case 'selection':
            selectionSort().then(() => {
                isSorting = false;
                drawArray();
            });
            break;
        case 'insertion':
            insertionSort().then(() => {
                isSorting = false;
                drawArray();
            });
            break;
        case 'cocktail':
            cocktailSort().then(() => {
                isSorting = false;
                drawArray();
            });
            break;
        case 'merge':
            mergeSort(array, 0, array.length - 1).then(() => {
                isSorting = false;
                drawArray();
            });
            break;
        case 'heap':
            heapSort().then(() => {
                isSorting = false;
                drawArray();
            });
            break;
        case 'quick':
            quickSort().then(() => {
                isSorting = false;
                drawArray();
            });
            break;
        case 'shell':
            shellSort().then(() => {
                isSorting = false;
                drawArray();
            });
            break;
        default:
            break;
    }
}